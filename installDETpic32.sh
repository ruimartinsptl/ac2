!/bin/bash
clear
echo -e "\e[32mOlá $USER!"
echo -e "\e[33mVai proceder à instalação do software para as aulas de AC2!"
echo "Software disponivel em:"
echo -e "\thttp://sweet.ua.pt/jla/"
echo ""
echo -e "\e[34mBugs do script: ruifilipe@ua.pt\e[0m"
echo -e "\e[101mV1.0e[0m"
echo -e "\e[32mPrima <enter> para continuar...\e[0m"
read
clear

sudo apt-get install -y wget geany vim
clear

MACHINE_TYPE=`uname -m`
if [ ${MACHINE_TYPE} == 'x86_64' ]; then
  echo -e "\e[32mA sua maquina é de 64 bits.\e[0m"
  rm /tmp/pic32-64-2013_10_29.tgz
  cd /tmp
  wget http://sweet.ua.pt/jla/Sw/pic32-64-2013_10_29.tgz
  sudo tar xzvf /tmp/pic32-64-2013_10_29.tgz -C /opt
  rm /tmp/pic32-64-2013_10_29.tgz
else
  echo -e "\e[32mA sua maquina é de 32 bits.\e[0m"
  rm /tmp/pic32-32-2013_10_29.tgz
  cd /tmp
  wget http://sweet.ua.pt/jla/Sw/pic32-32-2013_10_29.tgz
  sudo tar xzvf /tmp/pic32-32-2013_10_29.tgz -C /opt
  rm /tmp/pic32-32-2013_10_29.tgz
fi

sudo usermod -a -G dialout $USER

cd ~

if grep -Fxq "if [ -d /opt/pic32mx/bin ] ; then" .bashrc
then
	echo -e "\e[101mJá tem o .bashrc configurado!\e[0m"
else
	echo -e "\n\n##################################" >> .bashrc
	echo -e "# DETPIC32 Tools:" >> .bashrc
	echo -e "if [ -d /opt/pic32mx/bin ] ; then" >> .bashrc
	echo -e "\texport PATH=$PATH:/opt/pic32mx/bin" >> .bashrc
	echo -e "fi" >> .bashrc
	echo -e "##################################\n\n" >> .bashrc
fi

source ~/.bashrc

echo -e "\e[33mSoftware instalado!\e[0m"
echo ""

echo -e "\e[44mRelembro:\e[0m"
echo -e "\t\e[45mpcompile\e[0m para compilar"
echo -e "\t\t\e[43mExemplo:\e[0m pcompile superRato.c mr32.c"
echo -e "\t\e[45mldpic32\e[0m para transferir"
echo -e "\t\t\e[43mExemplo:\e[0m ldpic32 -w superRato.hex"
echo -e "\t\e[45mpterm\e[0m para abrir terminal para comunicar com o rato"
echo -e "\t\t\e[43mExemplo:\e[0m pterm"
echo ""
echo -e "\e[33mNo final reinicie a maquina, ou faça o load do ficheiro .bashrc com: source .bachrc\e[0m"
echo -e "\e[33mdone.\e[0m"
read

pterm